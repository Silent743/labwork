import math


AVERAGE_EARTH_R = float(6371)


class Coordinate(object):
    def __init__(self, value: float or int):
        # параметр соответствует координате в угловом формате,
        # что можно понять, посмотрев на метод in_degrees
        self._value = float(value)

    @property
    def in_degrees(self):
        return self._value

    @property
    def in_radian(self):
        raise NotImplementedError()

    # СТАТИЧЕСКИЙ МЕТОД
    @staticmethod
    def degrees_to_radian(value: float) -> float:
        # СТАТИЧЕСКИЙ Метод конвертации координаты (долго/широты) в радиальное обозначение.
        raise NotImplementedError()

    @staticmethod
    def radian_to_degrees(value: float) -> float:
        # СТАТИЧЕСКИЙ Метод конвертации координаты (долготы/широты) из радиан в угловые обозначения.
        return (value * float(180)) / math.pi


class GeoPoint(object):
    def __init__(self, lat, lon):
        self.lat = Coordinate(lat)
        self.lon = Coordinate(lon)

    def distance_to(self, end_point):
        # Расстояние от данной гео-метки до той, которая передана в параметре.
        raise NotImplementedError()

    @classmethod
    def from_location(cls, location):
        return cls(lat=location.lat, lon=location.lon)


def distance_between_points(start_point: GeoPoint, end_point: GeoPoint) -> float:
    """
    Возвращает расстояние между двумя метками на карте по их координатам,
    координаты указываются в десятичном виде. Пример десятичной записи
    (52.123987, 32.321789)


    :param start_point: Начальная точка.
    :param end_point: Конечная точка.
    :return: Расстояние между точками в КМ.
    """

    return AVERAGE_EARTH_R * math.acos(
        round(
            number=(
                    math.sin(start_point.lat.in_radian) * math.sin(end_point.lat.in_radian) +
                    math.cos(start_point.lat.in_radian) * math.cos(end_point.lat.in_radian) *
                    math.cos(start_point.lon.in_radian - end_point.lon.in_radian)
            ),
            ndigits=15
        )
    )