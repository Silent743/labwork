class Location(object):
    def __init__(self, obj):
        self.lat = float(obj.lat)
        self.lon = float(obj.lon)

    def getMsg(self):
        return f'Point coordinates is: lat: {self.lat}, lon: {self.lon}'
