import unittest
import math
from src.geo import Coordinate, distance_between_points, GeoPoint


class GeoTests(unittest.TestCase):
    def test_radian_to_degrees(self):
        radian = 1.5 * math.pi
        degrees_angle = Coordinate.radian_to_degrees(radian)
        self.assertEqual(degrees_angle, 270)

    def test_degrees_to_radian(self):
        degrees_angle = 270
        radian_angle = Coordinate.degrees_to_radian(degrees_angle)
        self.assertEqual(radian_angle, 1.5 * math.pi)

    def test_coordinate_in_radian(self):
        angle = 90
        coordinate = Coordinate(angle)
        self.assertEqual(coordinate.in_radian, 0.5 * math.pi)

    def test_distance_between_points(self):
        # 52.990264, 36.038662 Координаты ледовой Арены на наугорском Шоссе
        first_marker = GeoPoint(52.990264, 36.038662)
        # 52.982064, 36.051767 Координаты научприбора.
        second_marker = GeoPoint(52.982064, 36.051767)

        # С погрешностью в 10 метров
        delta_error = 0.01

        # Расстояние по google maps 1.27км
        expected_value = 1.27
        mean_value = distance_between_points(first_marker, second_marker)
        self.assertAlmostEqual(expected_value, mean_value, delta=delta_error)
